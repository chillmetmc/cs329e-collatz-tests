#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):

    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "1000000 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1000000)
        self.assertEqual(j, 1)
    
    def test_read_3(self):
        s = "-1 1"
        i, j = collatz_read(s)
        self.assertEqual(i, -1)
        self.assertEqual(j, 1)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)
    
    def test_eval_2(self):
        v = collatz_eval(250, 750)
        self.assertEqual(v, 171)
    
    def test_eval_3(self):
        v = collatz_eval(750, 250)
        self.assertEqual(v, 171)
    
    def test_eval_4(self):
        v = collatz_eval(1000, 1001)
        self.assertEqual(v, 143)
    
    def test_eval_5(self):
        v = collatz_eval(1001, 1000)
        self.assertEqual(v, 143)

    def test_eval_6(self):
        v = collatz_eval(2001, 3000)
        self.assertEqual(v, 217)
    
    def test_eval_7(self):
        v = collatz_eval(3000, 2001)
        self.assertEqual(v, 217)
    
    def test_eval_8(self):
        v = collatz_eval(1500, 2500)
        self.assertEqual(v, 209)
    
    def test_eval_9(self):
        v = collatz_eval(2500, 1500)
        self.assertEqual(v, 209)
    
    def test_eval_10(self):
        v = collatz_eval(999999, 999999)
        self.assertEqual(v, 259)
    
    # -----
    # print
    # -----
    
    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 20, 119)
        self.assertEqual(w.getvalue(), "100 20 119\n")
    
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 903, 1200, 182)
        self.assertEqual(w.getvalue(), "903 1200 182\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )

    def test_solve_2(self):
        r = StringIO("1 999999\n500 201\n326 50000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 999999 525\n500 201 144\n326 50000 324\n"
        )
    
    def test_solve_3(self):
        r = StringIO("999999 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "999999 1 525\n"
        )

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out

% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
